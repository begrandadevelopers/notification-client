<?php
namespace begranda\notificationclient;

class Notifications 
{

    public function storePost($apiKey,$userId,$title,$content,$tags,$avaibleFrom,$eventName,$source,$email,$buttonText=''){
        try {
            
                $url = "https://notificaciones.begranda.com/api/notification";

                $postData  = array(
                    'userId' => $userId, 
                    'title' => $title, 
                    'content' => $content, 
                    'tags' => $tags, 
                    'avaibleFrom' => $avaibleFrom, 
                    'eventName' => $eventName, 
                    'source' => $source, 
                    'readed' => 0, 
                    'email' => $email, 
                    'buttonText' => $buttonText, 
                );
                $ch = curl_init($url);
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization:Bearer '.$apiKey,
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postData)
                ));
            
                // Send the request
                $response = curl_exec($ch);
            
                // Check for errors
                if($response === FALSE){
                    die(curl_error($ch));
                }
            
                // Close the cURL handler
                curl_close($ch);

                return $responseData = json_decode($response, TRUE);

        } catch (\Throwable $th) {
            return $th;
        }
    }
    
}
